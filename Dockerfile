FROM openjdk:8-jre-alpine

RUN mkdir /app
COPY target/*.jar  /app/app.jar
CMD java $JAVA_OPTS -jar -Duser.timezone='America/Lima'  -Dspring.profiles.active=$ENV_PROFILE /app/app.jar